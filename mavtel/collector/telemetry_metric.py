import asyncio
import logging
from typing import Callable, Awaitable, Optional

from grpc import RpcError

from mavsdk import System
from mavtel_models import get_supported_metrics, MAVLinkMetricBaseModel

log = logging.getLogger(__name__)


class MAVLinkTelemetryMetric:
    def __init__(
            self,
            drone: System,
            name: str,
            on_update: Callable[[str, MAVLinkMetricBaseModel], Awaitable[None]] = None,
    ):
        self._drone: System = drone
        self._name: str = name
        self._on_update: Optional[Callable[[str, object], Awaitable[None]]] = on_update

        self._value: Optional[MAVLinkMetricBaseModel] = None
        self._metric_class: MAVLinkMetricBaseModel = get_supported_metrics()[name]

        self._started: bool = False
        self._collection_task: Optional[asyncio.Task] = None

    @property
    def name(self) -> str:
        return self._name

    @property
    def value(self) -> Optional[MAVLinkMetricBaseModel]:
        return self._value

    @property
    def started(self) -> bool:
        return self._started

    def metric_class(self) -> object.__class__:
        return self._metric_class

    def mavlink_data_class_name(self) -> str:
        return self._metric_class.get_mavlink_data_class_name()

    def __str__(self) -> str:
        return f'{self.__class__.__name__}( \n' \
               f'   name={self._name}, \n' \
               f'   started={self._started}, \n' \
               f'   value={self._value} \n' \
               f')'

    def start(self):
        if self._started:
            raise RuntimeError(f'{self} is already started')
        self._started = True
        self._collection_task = asyncio.create_task(self._collection_loop())

    def stop(self, wait: bool = False, cancel: bool = False):
        """
        Stops metric collection.
        :param wait: wait until stopped.
        :param cancel: force task cancellation
        """
        self._started = False

        if cancel:
            self._collection_task.cancel()
        if wait:
            asyncio.ensure_future(self._collection_task, loop=asyncio.get_running_loop())

        self._collection_task = None

    async def _collection_loop(self):
        while self._started:
            try:
                await self._collection_routine()
            except RpcError:
                log.error(f'RPC error at {self}, possibly a shutdown issue. Aborting...')
                break
            except Exception as err:
                log.exception(err)

    async def _collection_routine(self):
        log.info(f'Subscribe to drone "{self._name}" metric.')
        metric_stream = getattr(self._drone.telemetry, self._name)

        # Subscribe to metrics stream
        async for metric_value in metric_stream():
            # Abort if not started
            if not self._started:
                log.info(f'{self} is stopped, aborting metric collection...')
                return

            if metric_value is not None:
                self._value = self._metric_class.from_mavlink(metric_value)
            else:
                log.warning(f'Received None for "{self._name}", '
                            f'expected: {self.mavlink_data_class_name()}')

            # Call metric update handler
            if self._on_update is not None:
                asyncio.create_task(self._on_update(self._name, self._value))
