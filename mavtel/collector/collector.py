import asyncio
import logging
from typing import Awaitable, Callable, Dict, List, Optional, Set

from mavsdk import System
from mavtel.collector.telemetry_metric import MAVLinkTelemetryMetric
from mavtel.mavlink import MAVLinkConnection
from mavtel.storage.metrics_storage import MetricsStorage
from mavtel_models import get_supported_metrics, MAVLinkMetricBaseModel

log = logging.getLogger(__name__)

MetricUpdateHandler = Callable[[str, MAVLinkMetricBaseModel], Awaitable[None]]


class Collector:
    def __init__(
            self,
            mavlink: MAVLinkConnection,
            metrics_storage: MetricsStorage,
            metrics_to_track: Set[str] = None
    ) -> None:
        self._mavlink: MAVLinkConnection = mavlink
        self._metrics_storage: MetricsStorage = metrics_storage

        self._started: bool = False
        self._ready: bool = False
        self._drone: Optional[System] = None

        self._tasks: List[asyncio.Future] = []
        self._metrics: Optional[Dict[str, MAVLinkTelemetryMetric]] = None

        self._events_processed = 0

        self._handlers: Dict[str, MetricUpdateHandler] = dict()

        if metrics_to_track is None:
            self._metrics_to_track = get_supported_metrics()
        else:
            self._metrics_to_track = {
                metric_name: metric_class
                for metric_name, metric_class in get_supported_metrics().items()
                if metrics_to_track is None or metric_name in metrics_to_track
            }

    def is_started(self) -> bool:
        return self._started

    def is_ready(self) -> bool:
        return self._ready

    def get_processed_events_count(self) -> int:
        return self._events_processed

    def get_tracked_metrics(self) -> List[str]:
        return list(self._metrics_to_track.keys())

    def get_metric(self, name):
        if name in self._metrics_to_track:
            metric = self._metrics[name]
            return metric.value
        elif name in get_supported_metrics():
            log.warning(f'Metric "{name}" is not tracked!')
            return None
        else:
            raise KeyError(f"{self.__class__.__name__} has no metric '{name}'")

    def register_handler(self, name: str, handler: MetricUpdateHandler, recreate: bool = False):
        if name not in self._handlers.keys() or recreate:
            self._handlers[name] = handler
            log.info(f'Registered metrics update handler: "{name}".')

    def deregister_handler(self, name: str):
        if name in self._handlers:
            del self._handlers[name]

    def __getitem__(self, item):
        try:
            return self.get_metric(item)
        except KeyError as err:
            raise KeyError(f"{self.__class__.__name__} object has no key '{item}'") from err

    def __getattr__(self, item):
        try:
            return self.get_metric(item)
        except KeyError as err:
            raise AttributeError(f"{self.__class__.__name__} object has no attribute '{item}'") from err

    async def _updated(self):
        self._events_processed += 1

    async def _metric_update_handler(self, name: str, value: MAVLinkMetricBaseModel):
        for handler in list(self._handlers.values()):
            await handler(name, value)
        await self._updated()

    def _setup_metrics(self):
        if self._metrics is not None:
            raise RuntimeError(f'Metrics have been already configured!')

        # Iterate over metrics and setup telemetry
        self._metrics = {metric_name: MAVLinkTelemetryMetric(
            drone=self._drone,
            name=metric_name,
            on_update=self._metric_update_handler
        ) for metric_name in self._metrics_to_track.keys()}

    async def _activate_metrics(self):
        if self._metrics is not None:
            log.info(f'Tracking metrics: {list(self._metrics_to_track.keys())}.')
            for metric in self._metrics.values():
                metric.start()
        else:
            raise RuntimeError('Metrics are not configured. Nothing to activate.')

    def _deactivate_metrics(self):
        if self._metrics is not None:
            for metric in self._metrics.values():
                metric.stop()

    async def _reset_metrics(self):
        self._deactivate_metrics()
        self._setup_metrics()
        await self._activate_metrics()

    async def start(self):
        # At this moment we consider ourselves started but not ready
        self._started = True

        log.info('Starting collector...')
        self._drone = await self._mavlink.connect()

        log.info('Configuring metric collectors...')
        await self._reset_metrics()

        self._ready = True
        log.info('Collector is ready.')

    def stop(self):
        log.info('Stopping collector...')
        self._started = False
        self._ready = False

        log.info('Deactivating metrics collectors...')
        self._deactivate_metrics()

        log.info('Collector is stopped.')
