import asyncio
import logging

from mavtel.collector import Collector
from mavtel.lifecycle.app_services import AppServices
from mavtel.settings import settings

log = logging.getLogger(__name__)


def setup_collector(services: AppServices):
    collector = Collector(
        mavlink=services.mavlink,
        metrics_storage=services.storage,
        metrics_to_track=settings.METRICS,
    )
    services.collector = collector

    @services.app.on_event('startup')
    async def collector_background_start():
        log.info('Setting up collector...')
        asyncio.ensure_future(collector.start())

    @services.app.on_event('shutdown')
    async def collector_shutdown():
        collector.stop()
        log.info('Shutting down collector.')
