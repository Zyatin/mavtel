import asyncio
import logging

from mavtel.lifecycle.app_services import AppServices
from mavtel.mavlink.mavlink_connection import MAVLinkConnection
from mavtel.settings import settings

log = logging.getLogger(__name__)


def setup_mavlink(services: AppServices):
    mavlink = MAVLinkConnection(system_address=settings.MAVTEL_SYS_ADDR)
    services.mavlink = mavlink

    @services.app.on_event('startup')
    async def mavlink_delayed_start():
        log.info('Setting up MAVLink...')
        asyncio.ensure_future(mavlink.connect())

    @services.app.on_event('shutdown')
    async def mavlink_shutdown():
        mavlink.close()
        log.info('Shutting down MAVLink.')
