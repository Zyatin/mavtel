import logging
from typing import Dict, List

from mavtel.collector.collector import Collector
from mavtel.lifecycle.app_services import AppServices
from mavtel.settings import settings
from mavtel.storage.backends.base import BaseStorageBackend
from mavtel.storage.backends.dummy import DummyBackend
from mavtel.storage.backends.influxdb.influxdb import InfluxDBBackend
from mavtel.storage.metrics_storage import MetricsStorage

log = logging.getLogger(__name__)

ALLOWED_BACKENDS: Dict[str, BaseStorageBackend.__class__] = dict(
    imluxdb=InfluxDBBackend,
    dummy=DummyBackend,
)


def setup_storage(services: AppServices):
    log.info(f'InfluxDB database: {settings.INFLUXDB_URL}. Organization: "{settings.INFLUXDB_ORG}"')

    backends_list: List[str] = settings.STORAGE_BACKENDS
    main_backend = backends_list[0]

    if main_backend == 'dummy':
        log.warning('Main backend set to "dummy". This backend does not support history!')

    backends_set = set(backends_list)
    backends: Dict[str, BaseStorageBackend] = {}

    for backend_name in backends_set:
        if backend_name == 'influxdb':
            backends['influxdb'] = InfluxDBBackend(
                session=services.session,
                url=settings.INFLUXDB_URL,
                token=settings.INFLUXDB_TOKEN,
                org=settings.INFLUXDB_ORG,
                max_buffer_size=settings.INFLUXDB_BUFFER_SIZE,
                max_buffering_delay_ms=settings.INFLUXDB_BUFFER_DELAY_MS,
            )
        elif backend_name == 'dummy':
            backends['dummy'] = DummyBackend(session=services.session)
        else:
            raise RuntimeError(f'Backend "{backend_name}" is not supported!')

    log.info(f'Configured storage backends: {str(backends)}.')

    storage = MetricsStorage(
        backends=backends,
        main_backend=main_backend,
    )

    services.storage = storage

    @services.app.on_event('startup')
    async def setup_storage_on_start():
        log.info('Setting up metrics storage...')
        await storage.setup()

        collector: Collector = services.collector
        collector.register_handler(name='storage', handler=storage.on_metric_update)

    @services.app.on_event('shutdown')
    async def storage_shutdown():
        await storage.stop()
        log.info('Shutting down storage.')
