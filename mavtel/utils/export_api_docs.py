#!/usr/bin/env python
"""
Script to export the ReDoc documentation page into a standalone HTML file.

Thanks to https://github.com/Redocly/redoc/issues/726.
"""

import argparse
import json
import os

from mavtel.app import app

script_dir = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.dirname(os.path.dirname(script_dir))

HTML_TEMPLATE = """<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Gimbal Gamble - ReDoc</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://fastapi.tiangolo.com/img/favicon.png">
    <style>
        body {
            margin: 0;
            padding: 0;
        }
    </style>
    <style data-styled="" data-styled-version="4.4.1"></style>
</head>
<body>
    <div id="redoc-container"></div>
    <script src="https://cdn.jsdelivr.net/npm/redoc/bundles/redoc.standalone.js"> </script>
    <script>
        var spec = %s;
        Redoc.init(spec, {}, document.getElementById("redoc-container"));
    </script>
</body>
</html>
"""

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Export API docs')
    parser.add_argument('-d', '--dir', help='Export directory', default=root_dir)
    parser.add_argument('-f', '--file', help='Filename', default='api-docs.html')
    args = vars(parser.parse_args())

    dst = os.path.join(args['dir'], args['file'])
    content = app.openapi()

    with open(dst, "w") as fd:
        print(f'Exporting API docs to: {dst}...')
        print(HTML_TEMPLATE % json.dumps(content), file=fd)
