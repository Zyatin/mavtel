import logging

from mavtel.storage.backends.base import BaseStorageBackend
from mavtel_models.mavlink.base_metric_model import MAVLinkMetricBaseModel

log = logging.getLogger(__name__)


class DummyBackend(BaseStorageBackend):
    async def setup(self):
        self._is_started = True
        log.info(f'Setting up dummy backend.')

    async def save_telemetry(self, data_point: MAVLinkMetricBaseModel):
        """
        Saves telemetry for the current session.
        :param data_point: data point to save
        """
        pass

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f'{self.__class__.__name__}()'
