from typing import Dict, Union

from mavtel.services.session import Session
from mavtel_models.mavlink.base_metric_model import MAVLinkMetricBaseModel
from mavtel_models.typing import BackendConfigType


class BaseStorageBackend:
    def __init__(self, session: Session):
        self._session = session
        self._is_started: bool = False

    async def setup(self):
        self._is_started = True

    async def stop(self):
        self._is_started = False

    def is_started(self) -> bool:
        return self._is_started

    def is_ready(self) -> bool:
        return self._is_started

    async def save_telemetry(self, data_point: MAVLinkMetricBaseModel):
        """
        Saves telemetry for the current session.
        :param data_point: data point to save
        """
        raise NotImplemented

    def state(self) -> Dict[str, Union[bool, str, BackendConfigType]]:
        config: BackendConfigType = self.__dict__()
        return dict(
            is_started=self.is_started(),
            is_ready=self.is_ready(),
            backend_type=self.__class__.__name__,
            config=config,
        )

    def __dict__(self) -> BackendConfigType:
        return dict()
