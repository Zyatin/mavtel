import asyncio
import logging
from datetime import datetime, timedelta
from typing import List, Optional

from influxdb_client.client.bucket_api import BucketsApi
from influxdb_client.client.influxdb_client import InfluxDBClient
from influxdb_client.client.influxdb_client_async import InfluxDBClientAsync
from influxdb_client.client.write_api import Point
from influxdb_client.domain.write_precision import WritePrecision

from mavtel.services.session import Session
from mavtel.storage.backends.base import BaseStorageBackend
from mavtel_models.mavlink.base_metric_model import MAVLinkMetricBaseModel
from mavtel_models.typing import BackendConfigType

log = logging.getLogger(__name__)

SETUP_RETRY_TIMEOUT = 1.0
HEARTBEAT_TIMEOUT = 1.0


class InfluxDBBackend(BaseStorageBackend):
    def __init__(
            self,
            session: Session,
            url: str,
            token: str,
            org: str,
            max_buffer_size: int,
            max_buffering_delay_ms: int
    ):
        super().__init__(session=session)

        self._url: str = url
        self._token: str = token
        self._org: str = org
        self._default_tags = {}
        self._session: Session = session

        self._is_started: bool = False
        self._setup_task: Optional[asyncio.Task] = None
        self._is_healthy: bool = False
        self._healthcheck_task: Optional[asyncio.Task] = None

        self._max_buffer_size = max_buffer_size
        self._max_buffering_delay_ms = max_buffering_delay_ms

        self._buffer: List[Point] = []
        self._update_after: datetime = self._reset_update_timer()

    def is_started(self) -> bool:
        return self._is_started

    def is_ready(self) -> bool:
        if not self._is_started:
            return False
        if self._setup_task is None or not self._setup_task.done():
            return False
        return self._is_healthy

    async def setup(self):
        if self.is_ready():
            return
        self._is_started = True

        self._setup_task = asyncio.create_task(self._setup())
        self._healthcheck_task = asyncio.create_task(self._heartbeat())

    async def _setup(self):
        while self._is_started:
            log.info(f'Setting up InfluxDB database: {self._url}, organization: "{self._org}"')
            try:
                await self._ensure_bucket(self._get_session_bucket())
                self._is_healthy = True
                return
            except Exception as err:
                log.warning(f'Unable to setup InfluxDB: {err}')
                await asyncio.sleep(SETUP_RETRY_TIMEOUT)

    async def _heartbeat(self):
        while self._is_started:
            if not self._is_healthy:
                with self._get_client_sync() as client:
                    if client.ping():
                        log.info(f'Influxdb instance is now healthy. Buffer size: {len(self._buffer)}.')
                        self._is_healthy = True
                    else:
                        log.warning(f'InfluxDB is not healthy! Next check in {HEARTBEAT_TIMEOUT} seconds.')
            await asyncio.sleep(HEARTBEAT_TIMEOUT)

    async def stop(self):
        self._is_started = False
        self._setup_task = None
        log.info(f'InfluxDB database backend is stopped.')

    async def save_telemetry(self, data_point: MAVLinkMetricBaseModel):
        """
        Saves telemetry for the current session.
        :param data_point: data point to save
        """
        point = self._serialize_datapoint(data_point)
        self._buffer.append(point)

        if self._is_time_to_flush():
            await self._flush()

    def _is_time_to_flush(self) -> bool:
        size_condition = len(self._buffer) >= self._max_buffer_size
        time_condition = datetime.now() >= self._update_after
        return self._is_healthy and (size_condition or time_condition)

    def _reset_update_timer(self) -> datetime:
        self._update_after = datetime.now() + timedelta(milliseconds=self._max_buffering_delay_ms)
        return self._update_after

    async def _flush(self):
        _buffer = self._buffer.copy()
        self._buffer = []
        self._reset_update_timer()

        try:
            async with self._get_client() as client:
                write_api = client.write_api()
                await write_api.write(
                    bucket=self._get_session_bucket(),
                    record=_buffer,
                )
        except Exception as err:
            log.warning(f'Unable to save metrics to InfluxDB: {err}.')
            self._is_healthy = False
            self._buffer = _buffer + self._buffer

    def _serialize_datapoint(self, data_point: MAVLinkMetricBaseModel) -> Point:
        tags = self._default_tags.copy()
        metric_name = data_point.get_metric_name()

        point = Point.from_dict(dict(
            measurement=metric_name,
            tags=tags,
            fields=data_point.as_flat_dict(),
            time=datetime.utcnow(),
        ), write_precision=WritePrecision.MS)

        return point

    def _get_session_bucket(self) -> str:
        return self._session.name

    def _get_client(self) -> InfluxDBClientAsync:
        return InfluxDBClientAsync(url=self._url, token=self._token, org=self._org)

    def _get_client_sync(self) -> InfluxDBClient:
        return InfluxDBClient(url=self._url, token=self._token, org=self._org)

    async def _ensure_bucket(self, bucket_name: str):
        with self._get_client_sync() as client:
            buckets_api = BucketsApi(client)

            bucket = buckets_api.find_bucket_by_name(bucket_name=bucket_name)
            if bucket is None:
                log.info(f'Bucket "{bucket_name}" will be created.')
                buckets_api.create_bucket(bucket_name=bucket_name, org=self._org)
            else:
                log.info(f'Using exising bucket "{bucket_name}".')

    def __str__(self):
        return f'{self.__class__.__name__}({self.__dict__()})'

    def __repr__(self):
        return self.__str__()

    def __dict__(self) -> BackendConfigType:
        return dict(
            url=self._url,
            org=self._org,
            bucket=self._get_session_bucket(),
            max_buffer_size=self._max_buffer_size,
            max_buffering_delay_ms=self._max_buffering_delay_ms
        )
