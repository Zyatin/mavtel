from fastapi import APIRouter, Request, HTTPException
from fastapi.responses import JSONResponse, PlainTextResponse

from mavtel.collector.collector import Collector
from mavtel.storage.metrics_storage import MetricsStorage
from mavtel_models.api.app_ready_response import AppReadyResponse

router = APIRouter()


@router.get(
    "/check",
    response_class=PlainTextResponse
)
async def health_check(request: Request):
    """
    Standard health check.
    """
    if not request.app.services.collector.is_started():
        raise HTTPException(status_code=503, detail="Server is not started yet or going to shut down.")
    return PlainTextResponse("OK")


@router.get(
    "/ready",
    name="Server is ready",
    response_model=AppReadyResponse
)
async def ready(request: Request):
    """
    Readiness check.
    """
    storage: MetricsStorage = request.app.services.storage
    collector: Collector = request.app.services.collector

    is_ready = storage.is_ready() and collector.is_ready()
    status_code = 200 if is_ready else 503

    return JSONResponse(
        content=dict(
            is_ready=is_ready,
            is_storage_ready=storage.is_ready(),
            is_collector_ready=collector.is_ready()
        ),
        status_code=status_code
    )
