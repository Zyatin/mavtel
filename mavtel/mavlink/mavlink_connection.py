import asyncio
import logging
from typing import Optional

from mavsdk import System

log = logging.getLogger(__name__)


class MAVLinkConnection:
    def __init__(self, system_address: str = None):
        self._client: System = System()
        self._started: bool = False
        self._system_address: str = system_address or 'udp://:14540'

        self.__connection_task: Optional[asyncio.Task] = None

    def is_started(self) -> bool:
        return self._started

    def is_pending(self) -> bool:
        return self.__connection_task is not None and not self.__connection_task.done()

    async def _connect(self):
        log.info(f'Awaiting for connection to {self._system_address}...')
        await self._client.connect(system_address=self._system_address)
        log.info(f'Received connection from {self._system_address}.')

    async def _establish(self):
        if self.is_pending():
            await self.__connection_task

    async def connect(self) -> System:
        if self.__connection_task is None:
            self.__connection_task = asyncio.create_task(self._connect())
        await self._establish()

        self._started = True

        return self._client

    def close(self):
        log.info('Stopping UDP server...')
        del self._client

        self._client = System()
        self._system_address = None
        self._started = False
        self.__connection_task = None
