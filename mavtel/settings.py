import os
import subprocess
from enum import Enum
from typing import List, Optional, Union

from pydantic import BaseSettings, validator

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
ROOT_DIR = os.path.dirname(BASE_DIR)

_DEFAULT_BACKENDS = ['dummy']

try:
    with open(os.path.join(ROOT_DIR, '.VERSION')) as f:
        version = f.readlines()[0]
except FileNotFoundError:
    version = subprocess.check_output('git describe --tags --abbrev=0', cwd=ROOT_DIR, shell=True)

try:
    with open(os.path.join(ROOT_DIR, '.BUILD')) as f:
        build_id = f.readlines()[0]
except FileNotFoundError:
    build_id = 'local'


class LogLevels(str, Enum):
    DEBUG = "DEBUG"
    INFO = "INFO"
    WARNING = "WARNING"
    ERROR = "ERROR"


class Settings(BaseSettings):
    ALLOWED_ORIGINS: Union[List[str], str] = []
    LOG_LEVEL: LogLevels = LogLevels.INFO
    SERVICE_NAME: Optional[str] = 'MAVTel'

    # Build and version info
    MAVTEL_VERSION: Optional[str] = version
    MAVTEL_BUILD_ID: Optional[str] = build_id

    # Networking
    MAVTEL_WEB_PORT: Optional[int] = 8000
    MAVTEL_SYS_ADDR: Optional[str] = 'udp://:14540'

    # Urls for Swagger and ReDoc docs. If not provided - docs will be hidden.
    SWAGGER_DOCS_URL: Optional[str] = '/docs'
    REDOC_DOCS_URL: Optional[str] = '/redoc'

    # List of metrics
    # noinspection PyUnresolvedReferences
    METRICS: Optional[Union[List[str], str]] = None

    # List of backends
    STORAGE_BACKENDS: Union[List[str], str] = _DEFAULT_BACKENDS

    # InfluxDB config
    INFLUXDB_URL: Optional[str] = 'http://localhost:8086'
    INFLUXDB_TOKEN: Optional[str] = None
    INFLUXDB_ORG: str = 'mavtel'
    INFLUXDB_BUFFER_SIZE: Optional[int] = 200
    INFLUXDB_BUFFER_DELAY_MS: Optional[int] = 50

    # Session config
    SESSION_ID: Optional[str] = None

    # Hack to parse allowed origins as comma separated string from env vars.
    # Can be fixed once this PR is released: https://github.com/samuelcolvin/pydantic/pull/1848.
    # noinspection PyMethodParameters
    @validator("ALLOWED_ORIGINS", pre=True)
    def allowed_origins_parsing(cls, val):  # pylint: disable=no-self-argument, no-self-use
        if isinstance(val, str):
            return val.split(",")
        return val

    # Hack to parse storage backends as comma separated string from env vars.
    # Can be fixed once this PR is released: https://github.com/samuelcolvin/pydantic/pull/1848.
    # noinspection PyMethodParameters
    @validator("STORAGE_BACKENDS", pre=True)
    def backends_parsing(cls, val):  # pylint: disable=no-self-argument, no-self-use
        if val == '':
            return _DEFAULT_BACKENDS
        if isinstance(val, str):
            return val.split(",")
        return val

    # Hack to parse metrics as comma separated string from env vars.
    # Can be fixed once this PR is released: https://github.com/samuelcolvin/pydantic/pull/1848.
    # noinspection PyMethodParameters
    @validator("METRICS", pre=True)
    def metrics_parsing(cls, val):  # pylint: disable=no-self-argument, no-self-use
        if val == '':
            return None
        if isinstance(val, str):
            return val.split(",")
        return val

    class Config:
        env_file = os.path.join(ROOT_DIR, ".env")
        case_sensitive = True


settings = Settings()
