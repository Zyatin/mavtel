import random
import string
from datetime import datetime

RANDOM_POSTFIX_SIZE = 4


class Session:
    def __init__(self, session_id: str = None):
        self._session_start: datetime = datetime.utcnow()
        self._id: str = session_id or self.create_session_id()

    @property
    def id(self) -> str:
        return self._id

    @property
    def name(self) -> str:
        return f'session_{self._id}'

    @property
    def session_start(self) -> datetime:
        return self._session_start

    def create_session_id(self):
        datetime_part = self._session_start.isoformat()
        random_part = ''.join(
            random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits)
            for _ in range(RANDOM_POSTFIX_SIZE)
        )
        return f'{datetime_part}__{random_part}'
