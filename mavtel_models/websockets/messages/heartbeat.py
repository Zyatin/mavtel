from mavtel_models.websockets.messages.base_message import BaseWSMessage


class HeartbeatWSMessage(BaseWSMessage):
    pass
