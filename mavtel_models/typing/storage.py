from typing import Dict, Union, List

BackendConfigType = Dict[str, Union[str, int, float, List[str], List[int], List[float]]]
