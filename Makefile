###########################################################
# Developer's Guide
###########################################################
#
# All tasks should be explicitly marked as .PHONY at the
# top of the section.
#
# We distinguish two types of tasks: private and public.
#
# "Public" tasks should be created with the description
# using ## comment format:
#
#   public-task: task-dependency ## Task description
#
# Private tasks should start with "_". There should be no
# description E.g.:
#
#   _private-task: task-dependency
#

###########################################################
# Setup
###########################################################

# Include .env file
# Its contents will be overridden by the current environment
# variables (like dotenv should work).
ifneq (,$(wildcard ./.env))
	# Store current environment in a temporary file and include .env
	include $(shell env > /tmp/mavtel.env; echo .env)
	# Include variables from environment
	include /tmp/mavtel.env
	# Clean empty temporary file and include it again
	include $(shell echo > /tmp/mavtel.env; echo /tmp/mavtel.env)
    export
endif

###########################################################
# Project directories
###########################################################

root_dir := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
volumes_dir := $(root_dir)/.volumes
influxdb_dir := $(volumes_dir)/influxdb
artifacts_dir := $(root_dir)/.artifacts
coverage_dir := $(artifacts_dir)/coverage
test_results := $(artifacts_dir)/test-results.xml
docs_dir := $(root_dir)/docs
python_path := $(root_dir)
entrypoint_script := $(root_dir)/bin/entrypoint.sh

###########################################################
# Config
###########################################################

dotenv_paths := "$(root_dir)"
version := $(shell git describe --tags --abbrev=0)
build_id := local

###########################################################
# Help
###########################################################
.PHONY: help

help: ## Shows help
	@printf "\033[33m%s:\033[0m\n" 'Use: make <command> where <command> one of the following'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[32m%-18s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

###########################################################
# Initialization
###########################################################
.PHONY: init init-env install pull

init: init-env

init-env: ## Initializes .env files
	@echo "Creating .env files in $(dotenv_paths)..." && $(foreach dir, $(dotenv_paths), rsync -a -v --ignore-existing $(dir)/.env.template $(dir)/.env; )

reset-env: ## Resets .env files
	@echo "Deleting .env files in $(dotenv_paths)..." && $(foreach dir, $(dotenv_paths), rm -f $(dir)/.env; )

install: ## Install local dependencies
	@INSTALL_DEV=1 "$(root_dir)"/bin/install.sh

pull: ## Pulls Docker images
	@docker-compose pull

###########################################################
# Building
###########################################################
.PHONY: build

build: ## Build all services in Docker
	MAVTEL_VERSION=$(version) docker-compose build

###########################################################
# Running
###########################################################
.PHONY: up down mavtel mavtel-d mavtel-dev deps deps-d px4 px4-interactive influxdb

up: ## Starts all services in Docker
	docker-compose up

mavtel: ## Starts Gimbal Gamble in Docker
	docker-compose up mavtel

mavtel-d: ## Starts Gimbal Gamble in Docker (background)
	docker-compose up -d mavtel

mavtel-dev: ## Starts Gimbal Gamble locally
	@$(entrypoint_script) server --reload

deps: ## Starts all dependencies
	docker-compose up influxdb px4

deps-d: ## Starts all dependencies in background
	docker-compose up -d influxdb px4

px4: ## Starts PX4 gazebo
	docker-compose up px4

px4-interactive: ## Starts PX4 gazebo in interactive mode
	docker-compose run --rm --service-ports px4 -v typhoon_h480

influxdb: ## Starts influxdb metrics storage
	docker-compose up influxdb

vlc: ## Open video stream in VLC
	vlc rtsp://127.0.0.1:8554/live

down: ## Shuts down dockerized application and removes docker resources
	@docker-compose down --remove-orphans

###########################################################
# Development
###########################################################
.PHONY: generate

generate: ## Generates code
	@PYTHONPATH="$(root_dir)" python $(root_dir)/mavtel_client/_generate.py

###########################################################
# Testing
###########################################################
.PHONY: test tests-unit coverage test-mavtel-client

test: tests-unit coverage test-mavtel-client ## Run all tests

tests-unit: ## Runs unit tests
	coverage run \
		--source ./mavtel,./mavtel_models,./mavtel_client \
		-m py.test ./tests \
			--junit-xml $(test_results) || echo "Tests failed! Check '$(test_results)' for details."

coverage: ## Generates code coverage
	coverage html -d "$(coverage_dir)"

test-mavtel-client: ## Runs tests for MAVTel client
	docker-compose run --service-ports --rm \
		-e MAVTEL_URL=http://host.docker.internal:8000 \
		mavtel-client test

###########################################################
# Dependencies
###########################################################
.PHONY: lock pipenv-lock requirements

lock: pipenv-lock requirements ## Lock Python dependencies

pipenv-lock:  ## Lock Pipenv dependencies
	pipenv lock

requirements: ## Generate Python requirements
	@$(root_dir)/bin/generate-requirements.sh

###########################################################
# Deploy
###########################################################
.PHONY: publish-mavtel-client

publish-mavtel-client: ## Runs tests for MAVTel client
	docker-compose run --service-ports --rm \
		-e PYPI_TOKEN=$(PYPI_TOKEN) -e USE_PYPI_TEST_REPO=$(USE_PYPI_TEST_REPO) \
		mavtel-client publish

###########################################################
# Troubleshooting
###########################################################
.PHONY: check-ports fake-udp-client fake-udp-server

env: ## Prints environments variables
	@env

check-ports: ## Discovers process which opens UDP ports
	@echo "-----------------------------------------\n$(MAVTEL_WEB_PORT):"
	@lsof -i4TCP:$(MAVTEL_WEB_PORT) || echo "Free"
	@echo "-----------------------------------------\n$(PX4_MAIN_UDP_PORT)/udp:"
	@lsof -i4UDP:$(PX4_MAIN_UDP_PORT) || echo "Free"
	@echo "-----------------------------------------\n$(PX4_API_UDP_PORT)/udp:"
	@lsof -i4UDP:$(PX4_API_UDP_PORT) || echo "Free"
	@echo "-----------------------------------------"

fake-udp-client: ## Start fake UDP client
	socat - UDP:localhost:$(PX4_API_UDP_PORT)

fake-udp-server: ## Start fake UDP server
	socat UDP-RECV:$(PX4_API_UDP_PORT) STDOUT

###########################################################
# Documentation
###########################################################
.PHONY: docs api-docs

docs: api-docs ## Generate documentation

api-docs: ## Generate API docs
	@mkdir -p $(docs_dir)
	@PYTHONPATH=$(python_path) $(root_dir)/mavtel/utils/export_api_docs.py --dir=$(docs_dir)

###########################################################
# Cleaning
###########################################################
.PHONY: clean clean-python clean-coverage clean-influxdb clean-test-results

clean: down clean-python clean-coverage clean-test-results clean-influxdb ## Cleans environment

clean-python: ## Removes all Python cache
	$(root_dir)/bin/clean-python.sh

clean-test-results: ## Cleans test results
	rm -rf $(test_results)

clean-coverage: ## Cleans code coverage
	rm -rf $(coverage_dir)
	rm -f $(root_dir)/.coverage

clean-influxdb: ## Cleans code coverage
	rm -rf $(influxdb_dir)
