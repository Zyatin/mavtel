#!/usr/bin/env bash

set -e

script_dir="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";
root_dir="$(dirname "${script_dir}")";

pushd "${root_dir}"
echo "Cleaning Python cache and builds..."
find . | grep -E '(/.pytest_cache|/__pycache__|\.pyc$|\.pyo$|\.egg-info$)' | xargs rm -rf
rm -rf "${root_dir}/dist"
popd
