#!/usr/bin/env bash

set -e

function info-section {
   echo "-----------------------------------------------------------"
   echo "$1"
   echo "-----------------------------------------------------------"
}

script_dir="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";
root_dir="$(dirname "${script_dir}")";
libs_dir="${root_dir}/libs"
cd "${root_dir}"

install_dev=${INSTALL_DEV:-1}

_uname_sys_out="$(uname -s)"
case "${_uname_sys_out}" in
    Linux*)     system=Linux;;
    Darwin*)    system=Mac;;
    CYGWIN*)    system=Cygwin;;
    MINGW*)     system=MinGw;;
    *)          system="UNKNOWN:${_uname_sys_out}"
esac
echo "Installing library dependencies for '${system}'..."

if [ "${system}" == "Mac" ]; then
    info-section "Building MAVSDK-Python from source (workaround for '${system}' systems)..."
    "${libs_dir}/build-libs.sh"
fi

if [ "${install_dev}" == "1" ]; then
    info-section "Installing development Python dependencies..."
    pipenv sync --dev
else
    info-section "Installing only production Python dependencies..."
    pipenv sync
fi
