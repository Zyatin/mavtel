#!/usr/bin/env bash

set -e

function info-section {
   echo "-----------------------------------------------------------"
   echo "$1"
   echo "-----------------------------------------------------------"
}

script_dir="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";
root_dir="$(dirname "${script_dir}")";
libs_dir="${root_dir}/libs"
mavsdk_version=${MAVSDK_VERSION:-1.3.0}

cd "${libs_dir}"

_uname_sys_out="$(uname -s)"
case "${_uname_sys_out}" in
    Linux*)     system=Linux;;
    Darwin*)    system=Mac;;
    CYGWIN*)    system=Cygwin;;
    MINGW*)     system=MinGw;;
    *)          system="UNKNOWN:${_uname_sys_out}"
esac
echo "Installing library dependencies for '${system}'..."

###############################################################################
# MAVSDK-Python
###############################################################################
info-section "Recreating MAVSDK-Python repository"
rm -rf ./MAVSDK-Python
git clone -b "${mavsdk_version}" --single-branch --recursive https://github.com/mavlink/MAVSDK-Python.git

pushd MAVSDK-Python

#------------------------------------------------------------------------------
# Define architecture
#------------------------------------------------------------------------------
case "$(uname -m)" in
    armv6l*)    arch=armv6l;;
    armv7l*)    arch=armv7l;;
    arm64*)     arch=aarch64;;
    aarch64*)   arch=aarch64;;
    x86_64*)    arch=aarch64;;
    *)          arch="UNKNOWN"
esac

if [ "${arch}" = "UNKNOWN" ]; then
    echo "ERROR!!! Unknown architecture: $(uname -m)"
    exit 1
else
    echo "Found architecture: ${arch}"
fi

#------------------------------------------------------------------------------
# Install requirements
#------------------------------------------------------------------------------
pushd proto/pb_plugins
info-section "Installing protobufer plugins requirements..."
pip3 install -r requirements.txt
popd

info-section "Installing MAVSDK Python requirements..."
pip3 install -r requirements.txt -r requirements-dev.txt

#------------------------------------------------------------------------------
# Generate wrappers
#------------------------------------------------------------------------------
info-section "Generating wrappers..."
./other/tools/run_protoc.sh

#------------------------------------------------------------------------------
# Build
#------------------------------------------------------------------------------
info-section "Building MAVSDK-Python..."
export MAVSDK_SERVER_ARCH="${arch}"
python3 setup.py build

popd

pushd "${root_dir}"
  info-section "Installing MAVSDK-Python"

  echo "Making a back-up of Pipfile"
  rm -f Pipfile.bak && cp Pipfile Pipfile.bak
  echo "Installing local version of MAVSDK-Python..."
  pipenv install --ignore-pipfile --skip-lock -e ./libs/MAVSDK-Python || install_failed=1
  echo "Restoring Pipfile"
  rm -f Pipfile && cp Pipfile.bak Pipfile && rm Pipfile.bak

  # We want to throw an error if installation was not successful
  if [ "${install_failed}" == 1 ]; then
      echo "ERROR: Package install failed. Exiting with error!"
      exit 1
  fi
popd
