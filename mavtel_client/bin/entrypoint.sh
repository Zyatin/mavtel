#!/usr/bin/env bash

set -e

function info-section {
   echo "-----------------------------------------------------------"
   echo -e "$1"
   echo "-----------------------------------------------------------"
}

###############################################################################
# Setup
###############################################################################

script_dir="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )"
root_dir="$(dirname "$(dirname "${script_dir}")")"

task="${1:-test}"
declare -a args=( "${@:2}")

###############################################################################
# Settings
###############################################################################

use_pypi_test_repo="${use_pypi_test_repo:-False}"
pypi_token="${PYPI_TOKEN:-"insecure-token"}"

###############################################################################
# Init
###############################################################################

cd "${root_dir}"

###############################################################################
# Tasks
###############################################################################

function run-tests() {
  info-section "Running tests."
  py.test ./tests/mavtel_client
}

function publish-to-pypi() {
  if [ "${pypi_token}" = "insecure-token" ]; then
    echo "'PYPI_TOKEN' is not specified or invalid. Aborting."
    exit 1
  fi

  info-section "Install PyPi build dependencies."
  pip install --upgrade build twine

  info-section "Building for PyPi."
  python -m build

  if [ "${use_pypi_test_repo}" = "False" ]; then
    info-section "Publish to PyPi."
    twine upload --non-interactive --username __token__ --password "${pypi_token}" dist/*
  else
    info-section "Publish to PyPi (test repository)."
    twine upload --non-interactive --repository testpypi --username __token__ --password "${PYPI_TOKEN}" dist/*
  fi
}

function run-shell() {
  info-section "Executing shell command: ${args[*]}"
  "${args[@]}"
}

function show-help() {
  echo -e "Usage:
  test                      - runs tests
  publish                   - publish to PyPi
                              environment variables:
                                - 'PYPI_TOKEN' should be specified
                                - 'use_pypi_test_repo' - if True then release to http://test.pypi.org
  shell <shell command>     - executes shell command
  help                      - shows this help"
}

###############################################################################
# Execution
###############################################################################

case "${task}" in
  test)
    run-tests
    ;;
  publish)
    publish-to-pypi
    ;;
  shell)
    run-shell
    ;;
  help)
    show-help
    ;;
  *)
    echo "Unknown task: ${task}"
    show-help
    exit
esac
